# google-ml-crash-course

google machine learning crash course

`mlcc-exercises_en` google raw zip

`mlcc-exercises` start from zero


### Requirements
Python = 3.6.8
MacOS


### Installation
```
# python 3.7.2
pip uninstall tensorflow
python3 -m pip install --upgrade https://storage.googleapis.com/tensorflow/mac/cpu/tensorflow-1.12.0-py3-none-any.whl
pip install --ignore-installed --upgrade matplotlib pandas sklearn scipy seaborn notebook

# python 3.6.8
pip install --ignore-installed --upgrade tensorflow matplotlib pandas sklearn scipy seaborn notebook
```